<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | string | max:150',
            'lastname' => 'required | numeric|min:10|max:999999.99',
            'nick' => 'required | string | min:8',
            'email' => 'required | string | min:8',
            'rol' => 'required | string',
            'password' => 'required | string | min:8 '
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo obligatorio',
            'name.max' => 'Máximo: 150 caracteres',
            'lastname.numeric' => 'Ingresa solo números',
            'lastnam.min' => 'Mínimo: 10',
            'lastname.max' => 'Máximo: 999999.99',
            'nick.min' => 'min:8',
            'email.min' => 'min:8',
            'password' => 'min:8',
            'integer' => 'Opción inválida'
            ];
    }
}
