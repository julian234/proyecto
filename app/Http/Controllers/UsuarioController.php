<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;

class UsuarioController extends Controller
{
    public function show($id)
    {
        $usuario = User::find($id);
//        dd($usuario->role->name);
        return view('edit',compact('usuario'));
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'lastname' => 'required|min:1|max:100',
            'nick' => 'required|min:1|max:30',
            'email' => 'required|min:1|max:100',
            'rol' => 'required|min:1|max:10',
//            'role_id' => 'required|numeric',
            'password' => 'required|numeric|min:8',
        ];

        $this->validate($request, $rules);

        return back()->with('status','Datos cargados correctamente');
    }

    public function index()
    {
        $request->user()->authorizeRoles(['Coordinador', 'Reubicador']);
        $usuarios = User::All();
        return view('usuarios', compact('usuarios'));
    }

    public function index2()
    {
        //$request->user()->authorizeRoles(['user', 'admin']);
        $usuarios = User::All();
        return view('noAutorizado', compact('noAutorizado'));
    }

    public function edit(Request $request)
    {
        $usuario = User::find($request->id);
        $usuario->name = $request->name;
        $usuario->lastname = $request->lastname;
        $usuario->nick = $request->nick;
        $usuario->email = $request->email;
        $usuario->rol = $request->rol;
//        $usuario->role_id = $request->role_id;
        $usuario->password = Hash::make($request->password);
        $usuario->save();
        return redirect()->route('usuarios');
    }

    public function destroy(Request $request)
    {
        $usuarios=User::find($request->id)->delete();
        Session::flash('success2', 'se borro el usuario');
        return redirect()->route('usuarios')->with('success','Usuario eliminado satisfactoriamente');
    }


    public function findUsuario(Request $request)
    {
        $find = $request->input('search');
        $usuarioFind = User::where('name', 'like', '%'. $find .'%')->get();
        return view('findUsuario')->with(compact('usuarioFind','find'));
    }


    public function detalleUsuario(Request $request)
    {
        $usuario = User::find($request->id);
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->save();
        return view('detalleUsuario');
    }




    public function index3()
    {
        return view('noAutorizado');
    }

//    public belongsTofunction store(Request $request)
//    {
//        $rules = [
//            'name' => 'required|max:20',
//            'lastname' => 'required|numeric|min:1|max:10',
//            'nick' => 'required|numeric|min:1|max:10',
//            'email' => 'required|numeric|min:1|max:10',
//            'rol' => 'required|numeric|min:1|max:10',
//            'password' => 'required|numeric|min:8|max:20',
//        ];
//
//        $this->validate($request, $rules);
//
//        return back()->with('status','Datos cargados correctamente');
//    }

}
