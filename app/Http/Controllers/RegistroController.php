<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegistroController extends Controller
{
    public function show()
    {
        return view('registro');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'lastname' => 'required|min:1|max:100',
            'nick' => 'required|min:1|max:30',
            'email' => 'required|min:1|max:100',
            'rol' => 'required|min:1|max:10',
            'password' => 'required|numeric|min:8',
        ];

        $this->validate($request, $rules);

        $usuario = new User();

        $usuario->name = $request->name;
        $usuario->lastname = $request->lastname;
        $usuario->nick = $request->nick;
        $usuario->email = $request->email;

        $usuario->role_id = $request->rol;
        $usuario->password = Hash::make($request->password);
        $usuario->save();

        return back()->with('status','Datos cargados correctamente');
    }

    public function create(Request $request)
    {
        $usuario = new User();
        $usuario->name = $request->name;
        $usuario->lastname = $request->lastname;
        $usuario->nick = $request->nick;
        $usuario->email = $request->email;
        $usuario->rol = $request->rol;
//        $usuario->role_id = $request->role_id;
        $usuario->password = Hash::make($request->password);
        $usuario->save();

        //$usuario->roles()->attach(Role::where('name', 'user')->first());
        //return back();

        return back();
    }

    /*protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'lastname' =>$data['lastname'],
            'nick'=>$data['nick'],
            'email' => $data['email'],
            'rol'=>$data['rol'],
            'password' => Hash::make($data['password']),
        ]);
        $user->roles()->attach(Role::where('name', 'user')->first());
        return $user;
    }*/



    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }

        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}
