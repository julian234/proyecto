@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (Session::has('success2'))
                        <div class="alert alert-info">{{ Session::get('success2') }}</div>
                    @endif

                    <form class="form-busqueda" action="{{route('usuario.finder')}}" method="get">
                    {{-- @csrf --}}
                        <input class="input-busqueda" type="text" name="search" value="" placeholder="¿Qué estas buscando?" autofocus>
                        <button class="boton-lupa" type="submit" name="">
                        </button>
                    </form>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                    </thead>

                    <tbody>
                        @foreach($usuarios as $usuario)
                                <tr>
                                    <td>{{$usuario->id}}</td>
                                    <td><a href="/detalleUsuario/{{$usuario->id}}">{{$usuario->name}}</a></td>
                                    <td>{{$usuario->email}}</td>
                                    <td><a class="btn btn-primary btn-xs" href="/edit/{{$usuario->id}}" style="width:45%;"><span class="glyphicon glyphicon-pencil"><img src="images/lapiz.png" class="icono" title="Modificar" alt="Modificar" style="width:45%";></span></a></td>
                                    <td>

                                    <form action="/deleteuser" method="post">
                                        {{csrf_field()}}
                                        <input type="text" value='{{$usuario->id}}' style="display:none" name="id">
                                        <script type="text/javascript">
                                            function myFunction(event) {
                                            if (confirm("¿Seguro que quieres borrar al usuario?") == false) {
                                                event.stopPropagation();
                                                event.preventDefault();
                                                return false;
                                            }
                                            return true;
                                            }
                                        </script>
                                        <button class="btn btn-danger btn-xs" style="width:42%; position: relative; margin-top: -26%; margin-left: 62%" onclick="return myFunction(event), myFunction2();" href={{action('UsuarioController@destroy', $usuario->id)}}" type="submit"><img src="images/trash.png" class="icono" style="width:55%"; title="Eliminar" alt="Modificar"></span></button>
                                    </form>
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

