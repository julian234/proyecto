@extends('layouts.app')

@section('title', 'Buscar usuario')

@section('content')


  <div class="card-body">
    @forelse ($usuarioFind as $oneUsuario)
      <br><br>
      <a href="/detalleUsuario/{{$oneUsuario->id}}"><button class="btn btn-danger" style="margin-left:40%; width:30%"> {{ $oneUsuario->name }}</button></a>
      @empty
      <br><br>
        <h2>No hemos encontrado lo que estás buscando</h2>
      @endforelse
  </div>
<br>


@endsection
