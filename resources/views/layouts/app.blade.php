<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/templates.css" rel="stylesheet">
    <link href="/css/video-gallery.css" rel="stylesheet">
    <link href="/css/roboto-fontface.css" rel="stylesheet">
    <link href="/css/poncho.min.css" rel="stylesheet">

    <style>
        a#navbarDropdown.nav-link.dropdown-toggle:hover{
            /* background-color: white; */
            color: black;
        }

        Div.dropdown-menu.dropdown-menu-right.show{
            background-color: white;
            color: black;
        }

        .nav-item.dropdown.show{
            background-color: white;
            color: black;
            width: -40%;
        }

        .sobre:hover{
            background-color: grey;
        }


    </style>

</head>
<body>

    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/auth/login"><img src="/images/logoministerio.png" height="50" alt=""></a>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </div>


            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->

                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            
                        @endif

                        @else
                        
                        <li class="nav-item dropdown" style="position:absolute; margin-left:80%; margin-top:-5%">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="text-align: center">
                                {{ Auth::user()->name }} 
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="background-color:grey">
                                

                                <a class="dropdown-item" href="/registrarnuevousuario" style="margin-left:5%">Nuevo usuario</a>
                                
                                <a class="dropdown-item" href="/usuarios" style=" margin-left:10%">Usuarios</a>
                            
                                <a class="dropdown-item" href="{{ route('login') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" style=" margin-left:10%">
                                    {{ __('Logout') }}
                                </a>
                            
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            
                            </div>
                        </li>
                        {{-- <li style="position:relatie"><a href="/registrarnuevousuario">Nuevo usuario</a></li>
                        <br>
                        <li><a href="/usuarios">Usuarios</a></li> --}}
                    @endguest
                </ul>
            </div>

        </nav>
    </header>

    @yield('content')



    <br><br><br><br><br><br><br><br>
    <footer class="main-footer bg-primary" style="">
      <div class="container aligner">
        <div class="col-sm-6 ">
          <img class="image-responsive" src="images/logo-produccion-inverse.png" alt="Ministerio de Producción">
        </div>
        <div class="col-sm-6 text-right">
          <p class="text-muted small">Los contenidos de Produccion.gob.ar están licenciados bajo <a href="https://creativecommons.org/licenses/by/2.5/ar/">Creative Commons Reconocimiento 2.5 Argentina License</a></p>
        </div>
      </div>
    </footer>

    <script src="js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>
