@extends('layouts.app')

@section('title', 'Modificar usuario')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Modificar el usuario') }}</div>
                @include('messages')
                <div class="card-body">

                    <form action="/editarUsuario" method="post">
                    @csrf

                    <input type="text" value="{{$usuario->id}}" style="display:none" name="id">

                    <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" value="{{$usuario->name}}" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                    </div>

                    <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Apellido') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" value="{{$usuario->lastname}}" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="nick" class="col-md-4 col-form-label text-md-right">{{ __('Usuario') }}</label>

                                <div class="col-md-6">
                                    <input id="nick" value="{{$usuario->nick}}" type="text" class="form-control @error('nick') is-invalid @enderror" name="nick" value="{{ old('nick') }}" autocomplete="nick" autofocus>

                                    @error('nick')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" value="{{$usuario->email}}" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                        <select name="rol" style="margin-left:36%">
                                            <option value="Rubricador">Rubricador</option>
                                            <option value="Coordinador">Coordinador</option>
                                        </select>

                                        <div class="col-md-6">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                
                                        <div class="form-group row">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Password') }}</label>
                
                                            <div class="col-md-6">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                                            </div>
                                        </div>
                        <button type="submit" class="btn btn-success" style="margin-left:35%">Editar</button>
                    </form>

                    <a href="{{ route('usuarios') }}"><button type="submit" class="btn btn-danger" style="margin-left:35%; width:20%">Cancelar</button></a>


                    <form action="/deleteuser" method="post">
                        {{csrf_field()}}
                        <input type="text" value='{{$usuario->id}}' style="display:none" name="id">
                        <script type="text/javascript">
                            function myFunction(event) {
                            if (confirm("¿Seguro que quieres borrar al usuario?") == false) {
                                event.stopPropagation();
                                event.preventDefault();
                                return false;
                            }
                            return true;
                            }
                        </script>
                        <button class="btn btn-danger btn-xs" style="margin-left:35%; height:70%" title="Eliminar" onclick="return myFunction(event), myFunction2();" href={{action('UsuarioController@destroy', $usuario->id)}}" type="submit"><img src="images/trash.png" class="icono" style="width:45%"; title="Eliminar" alt="Eliminar"></span></button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection




{{-- <input type="text" value="{{$usuario->id}}" style="display:none" name="id">
<label for="name">Nombre</label>
<input type="text" value="{{$usuario->name}}" name="name" class="form-check" id="lastname">
<br><br>
<label for="lastname">Apellido</label>
<input type="text" value="{{$usuario->lastname}}" name="lastname" class="form-check" id="lastname">
<br><br>
<label for="nick">Nombre de usuario</label>
<input type="text" value="{{$usuario->nick}}" name="nick" class="form-check" id="nick">
<br><br>
<label for="email">Email</label>
<input type="email" value="{{$usuario->email}}" name="email" class="form-check" id="email">
<br><br> --}}
