@extends('layouts.app')

@section('title', 'Login')

@section('content')



<main role="main" class="img-cover" style="background-image: url(img/fondo-login-flamencos.jpg);">
    <div class="container-fluid">
        <div class="row">


            <div class="flex p-t-1 p-b-1">
                <div class="col-xs-12 col-md-4 text-center bg-white center-align holder">
                    <h5 class="p-t-2 p-b-2">Ingresá a tu cuenta</h5>
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">

                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group m-b-1">
                                    <input id="email" type="email" class="form-control input-sm @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail" autofocus >
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group m-t-0">
                                    <input id="password" type="password" class="form-control input-sm @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <!--Div para el reCaptcha-->
                                <div class="g-recaptcha" data-sitekey=""></div>

                                <button type="submit" class="btn btn-primary p-l-4 p-r-4">INICIÁ SESIÓN</button>
                                </br>
                                <small><a href="#">¿Olvidaste tu contraseña?</a></small>
                            </form>

                        </div>
                        <div class="col-xs-12 col-md 12 bg-gray p-t-1 p-b-2">
                            <h6 class="p-b-1"><strong>¿No tenés una cuenta?</strong></h6>
                            <a class="nav-link" href="{{ route('register') }}" style="color:white"><button type="submit" class="btn btn-success">{{ __('Register') }}</button></a>
                            <p><small><a href="#">Legales</a> &nbsp;|&nbsp; <a href="#">Términos y condiciones</a></small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>










{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
