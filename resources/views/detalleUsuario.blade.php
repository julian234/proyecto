@extends('layouts.app')

@section('title', 'Detalle de usuario')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Detalle del usuario') }}</div>

                <div class="card-body">
                    <form action="/editarusuario" method="post">
                    @csrf
                    <input type="text" value="{{$usuario->id}}" style="display:none" name="id">
                    <label for="name">Nombre</label>
                        <input type="text" value="{{$usuario->name}}" name="name" class="form-check" id="name">
                        <label for="email">Email</label>
                        <input type="email" value="{{$usuario->email}}" name="email" class="form-check" id="email">
                        <button type="submit" class="btn btn-success">Trabajar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
