@extends('layouts.app')

@section('title', 'Administracion de usuario')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Administrar usuarios</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (Session::has('success2'))
                            <div class="alert alert-info">{{ Session::get('success2') }}</div>
                        @endif

                        <form class="form-busqueda" action="{{route('usuario.finder')}}" method="get">
                            {{-- @csrf --}}
                            <input class="input-busqueda" type="text" name="search" placeholder="¿A quien buscas?" autofocus>
                            <button class="boton-lupa" type="submit" name="">Buscar
                            </button>
                        </form>

                        <table class="table">
                            <thead>
                            <tr>

                                <th><p>Nombre</p></th>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>

                                    <td0><a href="/detalleUsuario/{{$usuario->id}}">{{$usuario->name}}</a></td0>


                                    <td>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
