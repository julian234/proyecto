@extends('layouts.app')

@section('title', 'Administraci de usuario')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Administrar usuarios</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (Session::has('success2'))
                        <div class="alert alert-info">{{ Session::get('success2') }}</div>
                    @endif

                    <form class="form-busqueda" action="{{route('usuario.finder')}}" method="get">
                    {{-- @csrf --}}
                        <input class="input-busqueda" type="text" name="search" placeholder="¿A quien buscas?" autofocus>
                        <button class="boton-lupa" type="submit" name="">Buscar
                        </button>
                    </form>

                    <table class="table">
                        <thead>
                            <tr>

                                <th><p>Nombre</p></th>

                                {{-- <th><p style="margin-left:80%">Modificar</p></th>
                                <th><p style="margin-left:65%"> Eliminar</th> --}}
                            </tr>
                    </thead>

                    <tbody>
                        @foreach($usuarios as $usuario)
                                <tr>
                                    {{-- <td>{{$usuario->id}}</td> --}}
                                    <td><a href="/detalleUsuario/{{$usuario->id}}">{{$usuario->name}}</a></td>

                                    <td><a class="btn btn-primary btn-xs" href="/edit/{{$usuario->id}}" title="Modificar" style="width:20%; margin-left:150%;"><span class="glyphicon glyphicon-pencil"><img src="images/lapiz.png" class="icono" title="Modificar" alt="Modificar" style="width:45%";></span></a></td>
                                    <td>

                                    <form action="/deleteuser" method="post">
                                        {{csrf_field()}}
                                        <input type="text" value='{{$usuario->id}}' style="display:none" name="id">
                                        <script type="text/javascript">
                                            function myFunction(event) {
                                            if (confirm("¿Seguro que quieres borrar al usuario?") == false) {
                                                event.stopPropagation();
                                                event.preventDefault();
                                                return false;
                                            }
                                            return true;
                                            }
                                        </script>
                                        <button class="btn btn-danger btn-xs" style="width:26%; position: relative; margin-left:76%" title="Eliminar" onclick="return myFunction(event), myFunction2();" href={{action('UsuarioController@destroy', $usuario->id)}}" type="submit"><img src="images/trash.png" class="icono" style="width:45%"; title="Eliminar" alt="Modificar"></span></button>
                                    </form>
                                    </td>
                                </tr>
                        @endforeach


                        @if(Auth::user()->hasRole('admin'))
                            <div>Acceso como administrador</div>
                        @else
                            <div>Acceso usuario</div>
                        @endif
                        You are logged in!
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
