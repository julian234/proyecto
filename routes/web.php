<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

//Login - Register
Auth::routes();



//Agregar Usuario
Route::get('/registrarnuevousuario', 'RegistroController@show');
Route::post('/nuevousuario', 'RegistroController@store');



Route::get('/home', 'HomeController@index')->name('home');

//Los tres usuarios
Route::get('/usuarios', 'UsuarioController@index')->name('usuarios');
Route::get('/noAutorizado', 'UsuarioController@index3')->name('usuarios');


//Editar usuario
Route::get('/edit/{id}', 'UsuarioController@show');
Route::post('/editarUsuario', 'UsuarioController@edit');
Route::post('/editarUsuario', 'UsuarioController@store');






Route::post('deleteuser', 'UsuarioController@destroy');

Route::get('/detalleUsuario/{id}', 'UsuarioController@show');


Route::match(['get', 'post'], '/findUsuario', 'UsuarioController@findUsuario')->name('usuario.finder');

Route::get('/form', 'FormController@index');
Route::post('/form', 'FormController@store');
