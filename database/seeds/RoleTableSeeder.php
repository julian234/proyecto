<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'Coordinador';
        $role->description = 'Coordinador';
        $role->save();

        $role = new Role();
        $role->name = 'Reubicador';
        $role->description = 'Reubicador';
        $role->save();
    }
}
